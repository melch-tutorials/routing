import { LitElement, html } from '@polymer/lit-element'

class View4 extends LitElement {
  _render() { return html`<h2>View 4</h2>` }
}

window.customElements.define('view-4', View4);
